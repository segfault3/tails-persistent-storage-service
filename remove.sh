#!/bin/bash

set -eu

print_usage() {
  cat << EOF
usage: $0 [OPTIONS]

Options:
--system
    Remove the system service. The default is to remove the session service.
-h, --help
    Print this usage message.
EOF
}

# Parse arguments
while [ "$#" -gt 0 ]; do
case "$1" in
    -h|--help)
    print_usage
    exit 0
    ;;
    --system)
    SYSTEM=1
    shift
    ;;
    *)    # unknown option
    echo "unknown option: $1" >&2
    print_usage
    exit 1
    ;;
esac
done


DBUS_FILE="org.boum.tails.PersistentStorage.service"
SYSTEMD_FILE="test.service"
DBUS_CONFIG_FILE="org.boum.tails.PersistentStorage.conf"

if [ -n "${SYSTEM:-}" ]; then
  if [ "$EUID" -ne 0 ]; then
    echo "You must be root to remove a system service"
    exit 1
  fi

  # Delete system service
  rm -f "/usr/local/share/dbus-1/system-services/${DBUS_FILE}"
  rm -f "/etc/dbus-1/system.d/${DBUS_CONFIG_FILE}"
  rm -f "/etc/systemd/system/${SYSTEMD_FILE}"

  # Reload the systemd system unit file
  systemctl daemon-reload

  # Reload the D-Bus system service
  systemctl reload dbus
else
  # Delete session service
  rm -f "${HOME}/.local/share/dbus-1/services/${DBUS_FILE}"
  rm -f "${HOME}/.config/systemd/user/${SYSTEMD_FILE}"

  # Delete transient session service
  rm -f "$XDG_RUNTIME_DIR/dbus-1/services/${DBUS_FILE}"
  rm -f "$XDG_RUNTIME_DIR/systemd/user/${SYSTEMD_FILE}"

  # Reload the systemd user unit files
  systemctl --user daemon-reload

  # Reload the D-Bus service
  systemctl --user reload dbus
fi
