import gi
import logging
import os

gi.require_version('UDisks', '2.0')
from gi.repository import GLib, UDisks

from tps import executil
from tps import PERSISTENT_STORAGE_MOUNT_POINT

logger = logging.getLogger(__name__)

TAILS_MOUNTPOINT = "/lib/live/mount/medium"
PARTITION_GUID = "8DA63339-0007-60C0-C436-083AC8230908" # Linux reserved
PARTITION_LABEL = "TailsData"

# noinspection PyArgumentList
udisks = UDisks.Client.new_sync()  # type: UDisks.Client

class PartitionNotFoundError(Exception):
    pass

class InvalidPartitionError(Exception):
    pass

class PartitionNotUnlockedError(Exception):
    pass

class InvalidBootDeviceError(Exception):
    pass

class InvalidCleartextDeviceError(Exception):
    pass


class BootDevice(object):
    def __init__(self, udisks_object: UDisks.Object):
        self.udisks_object = udisks_object
        self.partition_table = \
            udisks_object.get_partition_table() # type: UDisks.PartitionTable
        self.block = self.udisks_object.get_block()
        if not self.block:
            raise InvalidBootDeviceError("Device is not a block device")
        self.device_path = self.block.props.device

    @classmethod
    def get_tails_boot_device(cls) -> "BootDevice":
        """Get the device which Tails was booted from"""

        system_partition = cls._get_underlying_block_device(TAILS_MOUNTPOINT)

        # E.g. optical drives have no org.freedesktop.UDisks2.Partition interface,
        # so let's try to find out the parent device, and fallback to the device
        # itself.
        # XXX: This doesn't make any sense to me. If the device is an optical
        #      drive, why would we want to try creating a persistent partition
        #      on it? Are there any valid cases in which we want to return the
        #      device itself if there is no parent device?
        partition = UDisks.Object.get_partition(
            system_partition)  # type: UDisks.Partition
        if partition:
            return BootDevice(udisks.get_object(partition.props.table))
        else:
            return BootDevice(system_partition)

    @classmethod
    def _get_underlying_block_device(cls, path: str) -> UDisks.Object:
        """Get the udisks object of the physical block device (e.g.
        /org/freedesktop/UDisks2/block_devices/sda1) on which the specified
        file is stored."""
        stat_res = os.stat(path)
        major = os.major(stat_res.st_dev)
        minor = os.minor(stat_res.st_dev)
        device_path = os.path.realpath(f"/dev/block/{major}:{minor}")
        device = os.path.basename(device_path)
        return udisks.get_object(
            f"/org/freedesktop/UDisks2/block_devices/{device}")

    def get_beginning_of_free_space(self) -> int:
        """Get the beginning of the free space on the device, in bytes"""
        # Get the partitions
        partitions = [udisks.get_object(p).get_partition()
                      for p in self.partition_table.props.partitions]
        # Get the ends of the partitions, in bytes
        partition_ends = [p.props.offset + p.props.size for p in partitions]
        # Return the end of the last partition, which is the beginning
        # of the free space.
        return max(partition_ends)


class Partition(object):
    """The Persistent Storage encrypted partition"""

    def __init__(self, udisks_object: UDisks.Object):
        self.udisks_object = udisks_object
        self.block = self.udisks_object.get_block()
        if not self.block:
            raise InvalidPartitionError("Device is not a block device")
        self.device_path = self.block.props.device
        self.partition = self.udisks_object.get_partition()
        if not self.partition:
            raise InvalidPartitionError(f"Device {self.device_path }is not a "
                                        f"partition")

    def get_cleartext_device(self) -> "CleartextDevice":
        """Get the cleartext device of Persistent Storage encrypted
        partition"""
        encrypted = self._get_encrypted()
        cleartext_device_path = encrypted.props.cleartext_device
        if cleartext_device_path == "/":
            raise PartitionNotUnlockedError(f"Device {self.device_path} is"
                                            f"not unlocked")
        return CleartextDevice(udisks.get_object(cleartext_device_path))

    def _get_encrypted(self) -> UDisks.Encrypted:
        """Get the UDisks.Encrypted interface of the partition"""
        encrypted = self.udisks_object.get_encrypted()
        if not encrypted:
            raise InvalidPartitionError(f"Device {self.device_path} is not "
                                        f"encrypted")
        return encrypted

    @classmethod
    def exists(cls) -> bool:
        """Return true if the Persistent Storage partition exists and
        false otherwise."""
        try:
            cls.find()
            return True
        except PartitionNotFoundError:
            return False

    @classmethod
    def find(cls) -> "Partition":
        """Return the Persistent Storage encrypted partition or raise
        a PartitionNotFoundError."""
        parent_device = BootDevice.get_tails_boot_device()
        partitions = parent_device.partition_table.props.partitions
        for partition_name in sorted(partitions):
            partition = udisks.get_object(partition_name)
            if partition.get_partition().props.name == PARTITION_LABEL:
                return Partition(partition)
        raise PartitionNotFoundError(f"Could not find partition with label "
                                     f"{PARTITION_LABEL} on "
                                     f"{parent_device.device_path}")

    @classmethod
    def create(cls, passphrase: str) -> "Partition":
        """Create the Persistent Storage encrypted partition"""
        parent_device = BootDevice.get_tails_boot_device()
        offset = parent_device.get_beginning_of_free_space()

        # Create and format the partition
        partition_table = parent_device.partition_table
        path = partition_table.call_create_partition_and_format_sync(
            arg_offset=offset,
            # Size 0 means maximal size
            arg_size=0,
            arg_type=PARTITION_GUID,
            arg_name=PARTITION_LABEL,
            arg_options=GLib.Variant('a{sv}', {}),
            arg_format_type="ext4",
            arg_format_options=GLib.Variant('a{sv}', {
                "label": GLib.Variant('s', PARTITION_LABEL),
                "encrypt.passphrase": GLib.Variant('s', passphrase),
            }),
        )

        # Wait for all UDisks and udev events to finish
        udisks.settle()
        executil.check_call(["/sbin/udevadm", "settle"])

        # Create the Partition object
        partition = Partition(udisks.get_object(path))

        # Mount the partition
        partition.get_cleartext_device().mount()

        # XXX: We don't have to do this when we mount it to
        #      /live/persistence/TailsData_unlocked manually, right?
        # Rename the cleartext device to "TailsData_unlocked" to be able
        # to mount the volume to /live/persistence/TailsData_unlocked
        # via live-boot.
        # See https://gitlab.tails.boum.org/tails/tails/-/issues/11529#note_58879
        # rename_dm_device(cleartext_device, "TailsData_unlocked")

        # Fix permissions
        # XXX: Why are those permissions required?
        fix_permissions(PERSISTENT_STORAGE_MOUNT_POINT)

        return partition

    def delete(self):
        """Delete the Persistent Storage encrypted partition"""
        # Ensure that the partition is unmounted
        self._ensure_unmounted()
        # Delete the partition. By setting tear-down to true, udisks
        # automatically locks the encrypted device if it is currently
        # unlocked.
        self.partition.call_delete_sync(arg_options=GLib.Variant('a{sv}', {
            "tear-down": GLib.Variant('b', True),
        }))

    def _ensure_unmounted(self):
        try:
            cleartext_device = self.get_cleartext_device()
        except (InvalidPartitionError, PartitionNotUnlockedError):
            # There is no cleartext device for this partition, so there
            # is nothing to unmount
            return

        try:
            cleartext_device.force_unmount()
        except GLib.GError as err:
            # Ignore errors caused by the device not being mounted
            if not err.matches(UDisks.error_quark(), UDisks.Error.NOT_MOUNTED):
                raise


class CleartextDevice(object):
    def __init__(self, udisks_object: UDisks.Object):
        self.udisks_object = udisks_object
        self.block = self.udisks_object.get_block()
        if not self.block:
            raise InvalidCleartextDeviceError("Device is not a block device")
        self.device_path = self.block.props.device

    @classmethod
    def is_mounted(cls):
        p = executil.run(["mountpoint", PERSISTENT_STORAGE_MOUNT_POINT])
        if p.returncode == 0:
            return True
        if p.returncode == 1:
            return False
        # If the return code is not 0 and not 1, something unexpected
        # happened, so we raise a CalledProcessException
        p.check_returncode()

    def mount(self):
        # Ensure that the mount point exists
        os.makedirs(PERSISTENT_STORAGE_MOUNT_POINT, mode=0o755, exist_ok=True)

        # Mount the Persistent Storage partition
        executil.check_call(
            ["mount", self.device_path, PERSISTENT_STORAGE_MOUNT_POINT],
        )

    def force_unmount(self):
        filesystem = self.udisks_object.get_filesystem()
        if not filesystem:
            # There is no filesystem, so there is nothing to unmount
            return
        # Unmount the filesystem until no mount points are left
        while filesystem.props.mount_points:
            filesystem.call_unmount_sync(arg_options=GLib.Variant('a{sv}', {
                "force": GLib.Variant('b', True),
            }))


def get_dm_name(obj: UDisks.Object) -> str:
    udisks_devno = obj.get_block().props.device_number
    out = executil.check_output(
        ["dmsetup", "ls", "-o", "devno"],
        text=True,
    )
    for line in out.strip().split("\n"):
        name, devno = line.split()
        if devno == f"({os.major(udisks_devno)}:{os.minor(udisks_devno)})":
            return name


def rename_dm_device(obj: UDisks.Object, new_name: str):
     dm_name = get_dm_name(obj)
     executil.check_call(["dmsetup", "rename", dm_name, new_name])


def fix_permissions(path: str):
    executil.check_call(["/bin/chmod", "0775", path])
    executil.check_call(["/bin/mount", "-o", "remount,acl", path])
    executil.check_call(["/bin/setfacl", "-b", path])
    executil.check_call(["/bin/setfacl", "-m",
                         "user:tails-persistence-setup:rwx", path])
