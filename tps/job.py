from logging import getLogger
import os
import threading
from typing import TYPE_CHECKING

from gi.repository import Gio, GLib

from tps.dbus import DBUS_BASE_PATH
from tps.dbus.object import DBusObject

if TYPE_CHECKING:
    from tps.service import Service

logger = getLogger(__name__)

job_id = 0
job_id_lock = threading.Lock()

class Job(DBusObject):
    """A job that can be cancelled. The job might be blocked by
    conflicting processes, in which case the ConflictingProcesses
    property should be set by the owner of the job."""

    dbus_info = '''
    <node>
        <interface name='org.boum.tails.PersistentStorage.Job'>
            <method name='Cancel'/>
            <property name="Id" type="u" access="read"/>
            <property name="ConflictingProcesses" type="au" access="read"/>
        </interface>
    </node>
    '''

    @property
    def dbus_path(self):
        return os.path.join(DBUS_BASE_PATH, "Jobs", str(self.Id))

    def __init__(self, service: "Service"):
        super().__init__()

        # Set the job ID
        global job_id
        job_id_lock.acquire()
        job_id += 1
        self.Id = job_id
        job_id_lock.release()

        logger.debug("Initializing job %r", self.Id)
        self.service = service

        self.cancellable = Gio.Cancellable.new()  # type: Gio.Cancellable
        self._conflicting_processes = list()

    def __enter__(self):
        self.register(self.service.connection)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.unregister(self.service.connection)

    # ----- Exported functions ----- #

    def Cancel(self):
        """Cancel the job"""
        logger.info(f"Cancelling job {self.Id}")
        self.cancellable.cancel()

    # ----- Exported properties ----- #

    @property
    def ConflictingProcesses(self) -> [int]:
        """A list if PIDs which have to be terminated before the
        job can continue"""
        return self._conflicting_processes

    @ConflictingProcesses.setter
    def ConflictingProcesses(self, pids: [int]):
        if self._conflicting_processes == pids:
            # Nothing to do
            return
        self._conflicting_processes = pids
        changed_properties = {"ConflictingProcesses": GLib.Variant("au", pids)}
        self.emit_properties_changed_signal(
            self.service.connection,
            "org.boum.tails.PersistentStorage.Job",
            changed_properties,
        )
