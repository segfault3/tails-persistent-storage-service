import logging
import time

from gi.repository import Gio, GLib

from tps.configuration import features
from tps.configuration.config_file import ConfigFile
from tps.dbus.object import DBusObject
from tps.dbus import DBUS_BASE_PATH
from tps.device import Partition, PartitionNotFoundError
from tps import PERSISTENT_STORAGE_MOUNT_POINT

logger = logging.getLogger(__name__)

SERVICE_NAME="org.boum.tails.PersistentStorage"

class AlreadyCreatedError(Exception):
    pass

class NotCreatedError(Exception):
    pass

class Service(DBusObject):
    dbus_info = '''
        <node>
            <interface name='org.boum.tails.PersistentStorage'>
                <method name='Quit'/>                
                <method name='Create'>
                    <arg name='passphrase' direction='in' type='s'/>
                </method>
                <method name='Delete'/>
                <method name='Activate'/>
                <property name="IsCreated" type="b" access="read"/>
            </interface>
        </node>
        '''

    dbus_path = DBUS_BASE_PATH

    def __init__(self, mainloop: GLib.MainLoop):
        super().__init__()
        self.mainloop = mainloop
        self.config_file = ConfigFile(PERSISTENT_STORAGE_MOUNT_POINT)
        self.bus_id = None
        self.connection = None
        self.features = list()  # type: ["Feature"]
        self._created = Partition.exists()

    # ----- Exported methods ----- #

    def Quit_method_call_handler(self, connection: Gio.DBusConnection,
                                 parameters: GLib.Variant,
                                 invocation: Gio.DBusMethodInvocation):
        """Terminate the Persistent Storage service"""
        # Make the D-Bus method return first, else our main thread
        # might exit before we can call return, resulting in a NoReply
        # error on the client.
        invocation.return_value(None)
        connection.flush_sync()
        logger.info("Quit() was called, terminating...")
        self.stop()

    def Create(self, passphrase: str):
        """Create the Persistent Storage partition and activate the
        default features"""
        if Partition.exists():
            raise AlreadyCreatedError(
                "The Persistent Storage was already created")

        Partition.create(passphrase)

        for feature in self.features:
            if feature.enabled_by_default:
                feature.do_activate(None, non_blocking=True)

        self.IsCreated = True

    def Delete(self):
        """Delete the Persistent Storage partition"""
        try:
            partition = Partition.find()
        except PartitionNotFoundError:
            raise NotCreatedError("The Persistent Storage was not yet created")
        partition.delete()
        self.IsCreated = False

    def Activate(self):
        """Activate all Persistent Storage features which are currently
        configured in the persistence.conf config file."""
        partition = Partition.find()
        if not partition:
            raise NotCreatedError("The Persistent Storage was not yet created")

        cleartext_device = partition.get_cleartext_device()
        if not cleartext_device.is_mounted():
            cleartext_device.mount()

        features_to_activate = \
            self.config_file.extract_features_to_activate(
                self.features)
        for feature in features_to_activate:
            feature.do_activate(None, non_blocking=True)

    # ----- Exported properties ----- #

    @property
    def IsCreated(self) -> bool:
        """Whether the Persistent Storage partition is created"""
        return self._created

    @IsCreated.setter
    def IsCreated(self, value: bool):
        if self._created == value:
            # Nothing to do
            return
        self._created = value
        changed_properties = {"IsCreated": GLib.Variant("b", value)}
        self.emit_properties_changed_signal(
            self.connection,
            'org.boum.tails.PersistentStorage',
            changed_properties,
        )

    # ----- Non-exported functions ----- #

    def on_bus_acquired(self, connection: Gio.DBusConnection,
                        name: str) -> None:
       logger.info(f"Bus {name} acquired")
       self.connection = connection
       self.register(connection)

       for Feature in features.get_classes():
           feature = Feature(self)
           feature.register(connection)
           self.features.append(feature)
       logger.debug("Done registering objects")

    @staticmethod
    def on_name_acquired(connection: Gio.DBusConnection, name: str) -> None:
        logger.info(f"Name {name} acquired")

    def on_name_lost(self, connection: Gio.DBusConnection, name: str) -> None:
        self.unregister(connection)
        logger.error(f"Lost name {name}, terminating...")
        # Quit the main loop, which causes our main thread to exit
        self.mainloop.quit()

    def start(self):
        """Start the Persistent Storage service"""
        # Some initialization is done in the on_bus_acquired() handler
        self.bus_id = Gio.bus_own_name(
            Gio.BusType.SYSTEM,
            SERVICE_NAME,
            Gio.BusNameOwnerFlags.ALLOW_REPLACEMENT |
            Gio.BusNameOwnerFlags.REPLACE,
            self.on_bus_acquired,
            self.on_name_acquired,
            self.on_name_lost,
        )

    def stop(self):
        """Stop the Persistent Storage service"""
        # Some cleanup is done in the on_name_lost handler
        Gio.bus_unown_name(self.bus_id)

    def settle(self):
        # Wait until all pending events on the main loop were handled
        context = self.mainloop.get_context()  # type: GLib.MainContext
        while context.iteration(may_block=False):
            logger.debug("Waiting for mainloop events to be handled")
            time.sleep(0.1)

    def save_config_file(self):
        """Save all currently active features to the config file"""
        active_features = [feature for feature in self.features
                           if feature.IsActive]
        self.config_file.save(active_features)

    def refresh_is_created(self) -> bool:
        """Refresh the IsCreated property, i.e. check if the
        Persistent Storage partition exists"""
        is_created = Partition.exists()
        self.IsCreated = is_created
        return is_created