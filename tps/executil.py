import logging
import subprocess
from typing import List

logger = logging.getLogger(__name__)

def run(cmd: List[str], *args, **kwargs) -> subprocess.CompletedProcess:
    logger.debug(f"Executing command {' '.join(cmd)}")
    return subprocess.run(cmd, *args, **kwargs)


def check_call(cmd: List[str], *args, **kwargs):
    logger.debug(f"Executing command {' '.join(cmd)}")
    subprocess.check_call(cmd, *args, **kwargs)


def check_output(cmd: List[str], *args, **kwargs) -> bytes or str:
    logger.debug(f"Executing command {' '.join(cmd)}")
    return subprocess.check_output(cmd, *args, **kwargs)
