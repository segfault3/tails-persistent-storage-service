import abc
from contextlib import contextmanager
from logging import getLogger
import os
import psutil
import subprocess
import threading
import time
from typing import TYPE_CHECKING

from gi.repository import GLib

from tps import PERSISTENT_STORAGE_MOUNT_POINT
from tps import executil
from tps.configuration.mount import Mount
from tps.dbus import DBUS_BASE_PATH
from tps.dbus.errors import ActivationFailedError, \
    AlreadyActivatedError, NotActivatedError, JobCancelledError, \
    PersistentStorageNotCreatedError
from tps.dbus.object import DBusObject
from tps.device import Partition, PartitionNotFoundError, \
    PartitionNotUnlockedError
from tps.job import Job

if TYPE_CHECKING:
    from tps.service import Service

logger = getLogger(__name__)

class ConflictingProcessesError(Exception):
    pass


class Feature(DBusObject, metaclass=abc.ABCMeta):
    dbus_info = '''
    <node>
        <interface name='org.boum.tails.PersistentStorage.Feature'>
            <method name='Activate'/>
            <method name='Deactivate'/>
            <property name="Id" type="s" access="read"/>
            <property name="IsActive" type="b" access="read"/>
            <property name="Job" type="o" access="read"/>
        </interface>
    </node>
    '''

    @property
    def dbus_path(self):
        return os.path.join(DBUS_BASE_PATH, "Features", self.Id)

    def __init__(self, service: "Service"):
        super().__init__()
        logger.debug("Initializing feature %r", self.Id)
        self.service = service
        self._job = None  # type: Job or None
        # Lock used to prevent race conditions when changing the job attribute
        self.job_lock = threading.Lock()

        # Check if the feature is active
        self._is_active = self.directories_mounted()

    # ----- Exported functions ----- #

    def Activate(self):
        # Check if we can activate the feature

        # XXX: We don't necessarily have to refresh the property here.
        #      If we find that the activation takes too long, this is something
        #      we could skip and use the cached property instead.
        is_created = \
            self.service.refresh_is_created()
        if not is_created:
            raise PersistentStorageNotCreatedError()

        # Check if feature is active
        is_active = self.refresh_is_active()
        if is_active:
            raise AlreadyActivatedError(
                "Feature %r is already activated" % self.Id)

        # If there is still a job running, cancel it
        if self._job:
            self._job.Cancel()

        # Create a job for the activation. We do this to allow the user
        # to cancel the job. During activation / deactivation, we wait
        # until all conflicting processes were terminated. Sometimes it
        # might not be possible / desirable for the user to terminate
        # conflicting processes, so we want to support cancellation.
        #
        # The simpler alternative would be to raise an error if any
        # conflicting processes are running and let the user
        # re-trigger the activation / deactivation once they
        # terminated the conflicting processes. But that would provide
        # worse UX, because a conflicting process might take quite
        # some time to terminate after the user closed the
        # corresponding application window. They would have to keep
        # re-triggering the activation / deactivation until the process
        # terminated.
        with self.new_job() as job:
            self.do_activate(job)

    def do_activate(self, job: Job, non_blocking=False):
        logger.info(f"Activating feature {self.Id}")

        pids = self.get_conflicting_pids()
        if pids and non_blocking:
            raise ConflictingProcessesError(
                f"Can't activate feature {self.Id}: Conflicting processes"
                f"are running ({' '.join(pids)})")
        elif pids:
            # Wait for conflicting processes to terminate. If the job is
            # cancelled by the user before the conflicting processes
            # terminate, an exception will be thrown, which will be passed
            # on to the client and handled there.
            self.wait_for_conflicting_processes_to_terminate(job)

        mounts = [f"{os.path.join(PERSISTENT_STORAGE_MOUNT_POINT, mount.src)}:" \
                  f"{mount.dest}" for mount in self.Mounts]
        executil.check_call(
            ["/usr/local/sbin/tails-persist", "activate"] + mounts,
        )

        # Check if the directories were actually mounted
        if not self.directories_mounted():
            raise ActivationFailedError("Activation of feature %r failed "
                                        "unexpectedly" % self.Id)

        self.IsActive = True

        # The feature was successfully activated, so now we try to save
        # the config file. If that fails, the feature will be left
        # active, which seems better than setting marking it as
        # inactive but leaving the mounts mounted.
        # XXX: Maybe we should instead unmount the mounts again and set
        # the feature to inactive if the config file save fails.
        self.service.save_config_file()

    def Deactivate(self):
        # Check if we can deactivate the feature

        is_created = \
            self.service.refresh_is_created()
        if not is_created:
            raise PersistentStorageNotCreatedError()

        # Check if feature is active
        is_active = self.refresh_is_active()
        if not is_active:
            raise NotActivatedError("Feature %r is not activated" % self.Id)

        # If there is still a job running, cancel it
        if self._job:
            self._job.Cancel()

        # Create a job for the deactivation. For rationale on why we
        # use a job here, see the comment in Activate().
        with self.new_job() as job:
            self.do_deactivate(job)

    def do_deactivate(self, job: Job):
        logger.info(f"Deactivating feature {self.Id}")

        # Wait for conflicting processes to terminate. If the job is
        # cancelled by the user before the conflicting processes
        # terminate, an exception will be thrown, which will be passed
        # on to the client and handled there.
        self.wait_for_conflicting_processes_to_terminate(job)

        mounts = [f"{os.path.join(PERSISTENT_STORAGE_MOUNT_POINT, mount.src)}:" \
                  f"{mount.dest}" for mount in self.Mounts]
        executil.check_call(
            ["/usr/local/sbin/tails-persist", "deactivate"] + mounts,
        )

        # Check if the directories were actually unmounted
        if self.directories_mounted():
            raise ActivationFailedError("Activation of feature %r failed "
                                        "unexpectedly" % self.Id)
        self.IsActive = False
        self.service.save_config_file()

    # ----- Exported properties ----- #

    @property
    def IsActive(self) -> bool:
        return self._is_active

    @IsActive.setter
    def IsActive(self, value: bool):
        if self._is_active == value:
            # Nothing to do
            return
        self._is_active = value
        changed_properties = {"IsActive": GLib.Variant("b", value)}
        self.emit_properties_changed_signal(
            self.service.connection,
            "org.boum.tails.PersistentStorage.Feature",
            changed_properties,
        )

    @property
    def Job(self) -> str:
        return self._job.dbus_path if self._job else "/"

    @Job.setter
    def Job(self, job: Job):
        self._job = job
        changed_properties = {"Job": GLib.Variant("s", self.Job)}
        self.emit_properties_changed_signal(
            self.service.connection,
            "org.boum.tails.PersistentStorage.Feature",
            changed_properties,
        )

    @property
    @abc.abstractmethod
    def Id(self) -> str:
        """The name of the feature. It must only contain the ASCII
        characters "[A-Z][a-z][0-9]_"."""
        return str()

    @property
    @abc.abstractmethod
    def Mounts(self) -> [Mount]:
        """A list of mounts, which are mappings of source directories
        to target paths. The source directories will be mounted to the
        target paths when the feature is activated."""
        return list()

    # ----- Non-exported properties ------ #

    @property
    @abc.abstractmethod
    def conflicting_executables(self) -> [str]:
        """A list of executable paths which must not be currently
        running when the feature is activated/deactivated."""
        return list()

    @property
    def enabled_by_default(self) -> bool:
        """Whether this feature should be enabled by default"""
        return False

    # ----- Non-exported functions ----- #

    def refresh_is_active(self) -> bool:
        is_active = self.directories_mounted()
        self.IsActive = is_active
        return is_active

    def directories_mounted(self) -> bool:
        """Returns true if all of this feature's directories are
        mounted from the Persistent Storage and false otherwise."""
        try:
            cleartext_device = Partition.find().get_cleartext_device()
        except (PartitionNotFoundError, PartitionNotUnlockedError):
            # The encrypted partition either doesn't exist or is not
            # unlocked, so none of this feature's directories can be
            # mounted
            return False

        for mount in self.Mounts:
            p = executil.run([
                "findmnt", f"--source={cleartext_device.device_path}",
                f"--target={mount.dest}"
            ],
            stdout=subprocess.DEVNULL)
            if p.returncode == 1:
                # The directory is not mounted
                return False
            # If the return code is not 0 and not 1, something unexpected
            # happened, so we raise a CalledProcessException
            p.check_returncode()

        # All source directories are mounted
        return True

    @contextmanager
    def new_job(self):
        with Job(self.service) as job:
            self.job_lock.acquire()
            self.Job = job
            self.job_lock.release()
            try:
                yield job
            finally:
                self.job_lock.acquire()
                # Set the job to None if no other job was set already
                if self.Job == job:
                    self.Job = None
                self.job_lock.release()

    def wait_for_conflicting_processes_to_terminate(self, job: Job):
        """Waits until all conflicting processes were terminated.
        Raises a JobCancelledError if the job was canelled while
        waiting."""

        # We tried to automatically find processes which use any of the
        # destination directories via lsof. We encountered some issues
        # with that:
        #  * lsof without any options can take a long time if there are
        #    a lot of active processes with a lot of open files.
        #  * lsof +D calls stat on each file in the directory, which 
        #    can also take a long time if the directory is large.
        #  * lsof +D furthermore exits with exit code 1 if any of the 
        #    files in the directory do *not* have any file use, which
        #    makes it hard to distinguish this (expected) case from
        #    actual error cases.
        #  * lsof +f with a mounted-on directory of a file system does
        #    not seem to list all files open on the file system.
        #    For example, this does not list the vim swap file:
        #       # vim /test
        #       # lsof +f -- / | grep test
        #    ... while this does:
        #       # vim /test
        #       # lsof -x +d / | grep test
        #
        # In the end, we decided to not automatically check for
        # processes using the destination directories, but only check
        # for those processes which should definitely not be running.

        conflicting_pids = self.get_conflicting_pids()

        # Set the conflicting processes, so that the frontend can tell
        # the user to close the corresponding applications
        job.ConflictingProcesses = conflicting_pids

        if not job.ConflictingProcesses:
            # There are no conflicting processes, so we don't have
            # to wait for anything
            return

        logger.info(f"Waiting for the user to terminate pids "
                    f"{conflicting_pids}")
        while job.ConflictingProcesses:
            if job.cancellable.is_cancelled():
                logger.info("Job was cancelled")
                raise JobCancelledError()

            for pid in job.ConflictingProcesses:
                if not psutil.pid_exists(pid):
                    logger.info(f"Conflicting process {pid} was terminated")
                    job.ConflictingProcesses.remove(pid)

            time.sleep(0.2)

        logger.info(f"All conflicting processes were terminated, "
                    f"continuing")
        return


    def get_conflicting_pids(self) -> [int]:
        conflicting_pids = list()
        for executable in self.conflicting_executables:
            conflicting_pids += [p.pid for p in psutil.process_iter()
                                 if p.exe() == executable]
        return conflicting_pids
