import inspect

from tps.configuration.mount import Mount
from tps.configuration.feature import Feature


class PersistentDirectory(Feature):
    Id = "PersistentDirectory"
    Mounts = [Mount("Persistent", "/home/amnesia/Persistent")]
    enabled_by_default = True
    conflicting_executables = []


class BrowserBookmarks(Feature):
    Id = "BrowserBookmarks"
    Mounts = [Mount("bookmarks", "/home/amnesia/.mozilla/firefox/bookmarks")]
    conflicting_executables = []


class NetworkConnections(Feature):
    Id = "NetworkConnections"
    Mounts = [Mount("nm-system-connections", "/etc/NetworkManager/system-connections")]
    conflicting_executables = []


class AdditionalSoftware(Feature):
    Id = "AdditionalSoftware"
    Mounts = [Mount("apt/cache", "/var/cache/apt/archives"),
              Mount("apt/lists", "/var/lib/apt/lists")]
    conflicting_executables = []


class Printers(Feature):
    Id = "Printers"
    Mounts = [Mount("cups-configuration", "/etc/cups")]
    conflicting_executables = []


class Thunderbird(Feature):
    Id = "Thunderbird"
    Mounts = [Mount("thunderbird", "/home/amnesia/.thunderbird")]
    conflicting_executables = []


class GnuPG(Feature):
    Id = "GnuPG"
    Mounts = [Mount("gnupg", "/home/amnesia/.gnupg")]
    conflicting_executables = []


class BitcoinClient(Feature):
    Id = "BitcoinClient"
    Mounts = [Mount("electrum", "/home/amnesia/.electrum")]
    conflicting_executables = []


class Pidgin(Feature):
    Id = "Pidgin"
    Mounts = [Mount("pidgin", "/home/amnesia/.purple")]
    conflicting_executables = ["/usr/bin/pidgin"]


class SSHClient(Feature):
    Id = "SSHClient"
    Mounts = [Mount("openssh-client", "/home/amnesia/.ssh")]
    conflicting_executables = []


class Dotfiles(Feature):
    Id = "Dotfiles"
    Mounts = [Mount("dotfiles", "/home/amnesia", uses_symlinks=True)]
    conflicting_executables = []


def get_classes():
    return [g for g in globals().values() if inspect.isclass(g)
            and Feature in g.__bases__]
