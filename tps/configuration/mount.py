import logging
import shlex

logger = logging.getLogger(__name__)

class Mount(object):
    """A mapping of a source directory to a target directory. When a
    feature is activated, all of its mounts are mounted, i.e. for each
    mount the source directory is mounted to the target directory.

    By default, the directory is mounted via a bind-mount. If
    uses_symlinks is true, instead of bind-mounting the directory,
    symlinks are created from each file in the source directory to the
    target directory.

    This corresponds to the "link" option of live-boot(5). Below is the
    description of that option from the live-boot(5) man page.

    Create the directory structure of the source directory on the persistence media in DIR
    and create symbolic links from the corresponding place in DIR  to  each  file  in  the
    source  directory.   Existing files or directories with the same name as any link will
    be overwritten. Note that deleting the links in DIR will only remove the link, not the
    corresponding  file  in  the  source;  removed  links will reappear after a reboot. To
    permanently add or delete a file one must do so directly in the source directory.

    Effectively link will make only files already in the source directory persistent,  not
    any  other files in DIR. These files must be manually added to the source directory to
    make use of this option, and they will appear in DIR  in  addition  to  files  already
    there.  This  option  is useful when only certain files need to be persistent, not the
    whole directory they're in, e.g. some configuration files in a user's home directory."""

    def __init__(self, src, dest, uses_symlinks=False):
        self.src = src
        self.dest = dest
        self.uses_symlinks = uses_symlinks

    def __str__(self):
        """The string representation of a mount. This is in the format
        of a persistence.conf line."""
        options = ','.join(shlex.quote(option) for option in self.options)
        return shlex.quote(self.dest) + '\t' + options

    def __eq__(self, other: "Mount" or str):
        """Check if the mount is equal to another mount or the string
        representation of another mount"""

        # Ensure that the other object is a string
        other = str(other)

        # Remove leading and trailing whitespace
        other = other.strip()
        print(f"other: {other}")
        # Get the white-space separated elements of the other object
        elements = shlex.split(other, comments=True)
        print(f"elements: {elements}")
        print(f"dest: {self.dest}")

        if len(elements) != 2:
            # The other object has an invalid number of white-space
            # separated elements
            return False

        if elements[0] != self.dest:
            # The destination doesn't match
            return False

        options = set(elements[1].split(','))
        if options != set(self.options):
            # The options don't match
            return False

        return True

    @property
    def options(self) -> [str]:
        options = [f"source={self.src}"]
        if self.uses_symlinks:
            options.append("link")
        return options
