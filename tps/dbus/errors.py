from abc import abstractmethod
import json

from gi.repository import Gio

class DBusError(Exception):
    """An exception that can be returned as an error by a D-Bus method"""
    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @classmethod
    def is_instance(cls, err) -> bool:
        if not Gio.DBusError.is_remote_error(err):
            return False
        return Gio.DBusError.get_remote_error(err) == cls.name


class ActivationFailedError(DBusError):
    name = "org.boum.tails.PersistentStorage.Error.ActivationFailed"

class AlreadyActivatedError(DBusError):
    name = "org.boum.tails.PersistentStorage.Error.AlreadyActivated"

class PersistentStorageNotCreatedError(DBusError):
    name = "org.boum.tails.PersistentStorage.Error.PersistentStorageNotCreated"

class NotActivatedError(DBusError):
    name = "org.boum.tails.PersistentStorage.Error.NotActivated"

class JobCancelledError(DBusError):
    name = "org.boum.tails.PersistentStorage.Error.JobCancelled"
